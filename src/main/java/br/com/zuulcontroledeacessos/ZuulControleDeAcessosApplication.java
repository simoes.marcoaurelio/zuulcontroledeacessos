package br.com.zuulcontroledeacessos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulControleDeAcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulControleDeAcessosApplication.class, args);
	}

}
